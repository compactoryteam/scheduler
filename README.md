# Appoint Master Scheduling App

### Here is a deployed staging version of application: ###

[Staging URL](https://apmscheduler.herokuapp.com/)

Tech stack: Angular 2 as front-end stack

To run locally:
npm run dev

To build for prod:
npm install
npm start