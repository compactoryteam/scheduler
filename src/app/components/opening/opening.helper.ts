export class OpeningHelper {
    // Format opening date
    formatDate(date: string): string {
        let d = new Date(date);
        let hh = d.getHours();
        let m = d.getMinutes().toString();
        let s = d.getSeconds().toString();
        let dd = 'AM';
        let h = hh;

        if (h >= 12) {
            h = hh - 12;
            dd = 'PM';
        }
        if (h === 0) {
            h = 12;
        }

        m = Number(m) < 10 ? '0' + m : m.toString();
        s = Number(s) < 10 ? '0' + s : s.toString();

        let hs = h < 10 ? '0' + h : h;
        let pattern = new RegExp('0?' + hh + ':' + m + ':' + s);

        let replacement = hs + ':' + m;
        replacement += ' ' + dd;

        return replacement;
    }

    // Return an array of disabled days for specified month
    getDisabledMonth(year: number, month: number): Array<any> {
        let disableDays: Array<any> = [];
        for (let i = 1; i <= 31; i++) {
            disableDays.push({
                year: year,
                month: month + 1,
                day: i
            });
        }

        return disableDays;
    }
}
