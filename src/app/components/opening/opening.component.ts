import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ValidationService } from '../../services/validationService';
import { AppointmentService } from '../../services/appointmentService';
import { SharedService } from '../../services/sharedService';

import { OpeningModel } from '../../models/openingModel';
import { Appointment } from '../../models/appointment';
import { Opening } from '../../models/opening';
import { OpeningHelper } from './opening.helper';

@Component({
  selector: 'opening-step',
  templateUrl: './opening.component.html'
})

export class OpeningComponent {
    @Output() onStepReady: EventEmitter<any> = new EventEmitter();

    appointment: Appointment;
    availableOpenings: Array<Opening> = new Array<Opening>();
    oHelper: OpeningHelper;

    // model to load openings
    openings: Array<Opening> = new Array<Opening>();
    token: string = '';
    inProgress: Boolean = true;
    isSelected: Boolean = false;
    showSpinner: Boolean = true;
    isError: Boolean = false;
    errorText: string = '';
    // model to load date picker
    dpOptions: any;
    selectedDate: any;
    max: number = -1;
    showText: Boolean = true;

    // model to load opening view
    oViewModel: any;
    subscription: Subscription;

    constructor(private apmService: AppointmentService,
                private validationService: ValidationService,
                private sharedService: SharedService) {
                    this.subscription = this.sharedService.appointment$.subscribe(this.getAppointment.bind(this));
                    this.oHelper = new OpeningHelper();
                }

    ngOnInit() {
        // init values
        let selectedDate = new Date();
        this.selectedDate = null;
        this.dpOptions = this.getOptions();
        this.setDisabledDaysOptions(selectedDate.getFullYear(), selectedDate.getMonth());

        if (this.appointment && this.appointment.reason) {
            if(!this.appointment.opening) {
                this.appointment.opening = new Opening();
            } else {
                selectedDate = new Date(this.appointment.opening.dateTime)
                this.selectedDate = this.toDatePickerDate(selectedDate);
            }

            this.loadOpenings(this.appointment.reason.id, selectedDate, this.token);
        }
    }

    toDatePickerDate(date: Date): any {
        return {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate()
        }
    }

    toNormalDate(date: any): Date {
        return date ? new Date(date.year, date.month - 1, date.day) : new Date();
    }

    getOptions(): any {
        let disabledUntil = this.toDatePickerDate(new Date());

        return {
            dateFormat: 'yyyy-mm-dd',
            firstDayOfWeek: 'mo',
            sunHighlight: false,
            editableMonthAndYear: false,
            height: '34px',
            width: '260px',
            inline: true,
            disableUntil: disabledUntil,
            selectionTxtFontSize: '16px',
            disableDays: []
        };
    }

    setOptions(options: any): void {
        this.dpOptions = options;
    }

    loadOpenings(reasonId: number, currDate: Date, token: string): void {
        if (!this.inProgress) {
            var date = new Date();
            this.setDisabledDays(date.getFullYear(), date.getMonth());
            this.inProgress = true;
            return;
        }

        let that = this;
        let fromDate = new Date(currDate.toString());
            fromDate.setDate(1);
        let toDate = new Date(fromDate.toString());
            toDate.setMonth(toDate.getMonth() + 1);
            toDate.setDate(1);

        this.openings = new Array<Opening>();
        this.apmService.getOpenings(reasonId, fromDate, toDate, token).then(function(resp: OpeningModel) {
            that.openings = that.openings.concat(resp.openings);
            that.token = resp.continuationToken;
            that.inProgress = resp.inProgress;

            that.filterOpenings(that.toNormalDate(that.selectedDate));

            setTimeout(function() {
                that.loadOpenings(that.appointment.reason.id, currDate, that.token);
            }, 500);
        }, function(error: any) {
            that.isError = true;
            that.errorText = 'Something wrong happened with your request. Please retry or contact us in case you will see this message again.'
            console.log(error);
        });
    }

    filterOpenings(date: Date): void {
        this.availableOpenings = new Array<Opening>();
        for (let i = 0; i < this.openings.length; i++) {
            let oDate = new Date(this.openings[i].dateTime);
            if (oDate && date && oDate.toDateString() === date.toDateString()) {
                this.availableOpenings.push(this.openings[i]);
            }
        }

        this.renderOpenings();
        this.isSelected = true;
    }

    renderOpenings(): void {
        this.oViewModel = {
            morning: new Array<Opening>(),
            afternoon: new Array<Opening>(),
            evening: new Array<Opening>(),
        };

        for (let i = 0; i < this.availableOpenings.length; i++) {
            let op = this.availableOpenings[i];

            let av = true;
            for (let j = i + 1; j < this.availableOpenings.length; j++) {
                if (op.dateTime === this.availableOpenings[j].dateTime) {
                    av = false;
                    break;
                }
            }

            if (!av) continue;

            let oTime = new Date(op.dateTime);
            let afternoonTime = new Date(op.dateTime);
            let eveningTime = new Date(op.dateTime);

            afternoonTime.setHours(12);
            afternoonTime.setMinutes(0);
            eveningTime.setHours(16);
            eveningTime.setMinutes(0);

            op.viewString = this.oHelper.formatDate(op.dateTime.toString());

            if (oTime <= afternoonTime) {
                this.oViewModel.morning.push(op);
            } else if (oTime <= eveningTime) {
                this.oViewModel.afternoon.push(op);
            } else {
                this.oViewModel.evening.push(op);
            }
        }
    }

    onDateChanged(event: any): void {
        if(!event.formatted) return;
        this.showSpinner = true;
        this.showText = false;
        this.selectedDate = this.toDatePickerDate(event.jsdate);
        let date = event.jsdate;
        this.filterOpenings(date);
    }

    onCalendarViewChanged(event: any): void {
        if(event.changes && event.changes.options) return;
        this.isSelected = false;
        this.showText = false;
        if(this.selectedDate && event.month !== this.selectedDate.month)
            this.showText = true;

        let d = new Date(event.year, event.month - 1, 1);
        this.loadOpenings(this.appointment.reason.id, d, '');

        this.showSpinner = true;
    }

    getOpeningProviders(opening: Opening): Array<any> {
        let providers = new Array<any>();

        for (let j = 0; j < this.availableOpenings.length; j++) {
            if (opening.dateTime === this.availableOpenings[j].dateTime && this.availableOpenings[j].providerId) {
                providers.push(this.availableOpenings[j]);
            }
        }

        var uniqueArray = this.removeDuplicates(providers, "providerId");
        return uniqueArray;
    }

    removeDuplicates(arr: Array<any>, prop: string) {
        let new_arr = new Array<any>();
        let lookup  = {};

        for (var i in arr) {
            lookup[arr[i][prop]] = arr[i];
        }

        for (i in lookup) {
            new_arr.push(lookup[i][prop]);
        }

        return new_arr;
    }

    selectOpening(opening: Opening) {
        let item = this.appointment ? this.appointment : JSON.parse(sessionStorage.getItem('appointment'));
        item.opening = opening;
        
        item.opening.text = new Date(opening.dateTime).toLocaleString();
        item.providers = this.getOpeningProviders(opening);
        sessionStorage.setItem('appointment', JSON.stringify(item));

        this.onStepReady.emit({ number: 3, apt: item });
        this.sharedService.shareAppointment(item);
    }

    setDisabledDays(year: number, month: number): void {
        let disable: Array<any> = [];
        for (let m = 0; m < 12; m++) {
            month = m;
            //get disabled days
            let disableDays = this.setDisabledDaysOptions(year, month);

            let allowedOpenings: Array<Opening> = [];
            for (let i = 0; i < this.openings.length; i++) {
                let opDate = new Date(this.openings[i].dateTime);
                if (opDate.getMonth() === month) {
                    allowedOpenings.push(this.openings[i]);
                    let day = opDate.getDate();
                    disableDays[day - 1] = null;
                }
            }

            for (let i = 0; i < disableDays.length; i++) {
                if (disableDays[i]) {
                    disable.push(disableDays[i]);
                }
            }
        }

        let options = this.getOptions();
        options.disableDays = disable;
        this.dpOptions = options;

        this.showSpinner = false;
        this.onDateChanged({ jsdate: this.toNormalDate(this.selectedDate) });
    }

    getAppointment(msg: string): void {
        if (msg) {
            this.appointment = JSON.parse(msg);
        }
    }

    setDisabledDaysOptions(year: number, month: number): Array<any> {
        let disabled = this.oHelper.getDisabledMonth(year, month);
        let options = this.getOptions();
        options.disableDays = disabled;

        this.setOptions(options);

        return disabled;
    }
}
