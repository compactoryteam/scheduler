import { Input, Output, AfterContentInit, ContentChild,AfterViewInit, EventEmitter, Component, ViewChild, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { AppointmentService } from '../../services/appointmentService';
import { SharedService } from '../../services/sharedService';
import { ValidationService } from '../../services/validationService';
import { MaskHelper } from '../../helpers/maskHelper';

import { Appointment } from '../../models/appointment';
import { Person } from '../../models/person';
import { Settings } from '../../models/settings';
import { Account } from '../../models/account';
//import * as $ from 'jquery';
declare let $: any;
declare let Inputmask: any;

import { CustomValidators } from '../../helpers/validationHelper';

@Component({
  selector: 'person-step',
  templateUrl: './person.component.html'
})

export class PersonComponent {
    @ViewChildren('input') vc: any;

    @Output() onStepReady: EventEmitter<any> = new EventEmitter();
    subscription: Subscription;
    settingsSub: Subscription;
    accountSub: Subscription;
    appointment: Appointment;
    selectedDate: any;
    settings: Settings;
    account: Account;
    person: Person = new Person();
    personFormGroup: FormGroup;
    securityFormGroup: FormGroup;
    passwordFormGroup: FormGroup;
    currentView: string = 'form-person';
    personDataView: any;
    securityErrorMessage: string = '';
    passwordErrorMessage: string = '';
    globalError: string = '';
    personToken: string = '';
    redirectUrl: string = 'http://appointmaster.wixsite.com/appointmaster/scheduling';
    states: Array<any> = new Array<any>();
    showRedirectMessage: boolean = false;

    viewModel: any = {
      counter: 500,
      maxLength: 500
    };

    validations: any = {
        phoneError: '',
        stateError: '',
        zipError: '',
        birthDateError: '',
        oneRequiredError: ''
    };

    formats: any = {
        phoneFormat: '',
        zipCodeFormat: '',
        stateFormat: ''
    };

    sValidations: any = {
        phoneError: ''
    };

    dpOptions: any = {};
    selectedState: string = "";
    activeState: Array<any> = new Array<any>();

    constructor(private apmService: AppointmentService,
                private sharedService: SharedService,
                private validationService: ValidationService) {
                    this.subscription = this.sharedService.appointment$.subscribe(this.getAppointment.bind(this));
                    this.settingsSub = this.sharedService.settings$.subscribe(this.getSettings.bind(this));
                    this.accountSub = this.sharedService.account$.subscribe(this.getAccount.bind(this));
                }

    ngOnInit() {
      this.initForm(this.settings);
        let date = new Date();

        this.selectedDate = this.person.birthDate ? this.person.birthDate.formatted : '';

        this.dpOptions = {
            dateFormat: this.account.countryCode === 'GB' || this.account.countryCode === 'NZ' ? 'dd-mm-yyyy' : 'mm-dd-yyyy',
            firstDayOfWeek: 'mo',
            showClearDateBtn: false,
            showTodayBtn: false,
            height: '34px',
            width: '260px',
            selectionTxtFontSize: '14px',
            disableDays: [],
            editableDateField: true,
            customPlaceholderTxt: 'Select birth date',
            inputValueRequired: this.settings.personDateOfBirthRequired,
            disableSince: { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() }
        };

        let states = this.settings.countryDataSettings.states;
        if (states) {
            for (let i = 0; i < states.length; i++) {
                if (states[i].value === '') states[i].value = '-1';
                this.states.push({ id: states[i].value, text: states[i].displayValue });
                if (this.states[i].id === this.person.state) {
                    this.activeState.push(this.states[i]);
                }
            }
        }
    }

    ngAfterViewInit() {
        console.log(this.vc);
        this.vc.first.nativeElement.focus();

        let mh = new MaskHelper();
        this.formats.phoneFormat = mh.convertToNormalFormat(this.settings.countryDataSettings.phoneFormat);
        this.formats.stateFormat = mh.convertToNormalFormat(this.settings.countryDataSettings.stateFormat);
        this.formats.zipCodeFormat = mh.convertToNormalFormat(this.settings.countryDataSettings.zipCodeFormat);

        Inputmask.extendDefaults({ 'autoUnmask': true });
        let that = this;
        $("#personPhone").inputmask({ mask: this.formats.phoneFormat, definitions: { '9': { validator: '[0-9]' }, 'a': { validator: '[a-zA-Z]' } } });
        $("#personPhone").on('keyup', function(evt: any) {
            let input = that.personFormGroup.get('phone');
            input.setValue(this.value);
            that.person.phone = this.value;
        });

        $("#personState").inputmask({ mask: this.formats.stateFormat, definitions: { 'a': { validator: '[a-zA-Z0-9]' } } });
        $("#personState").on('keyup', function(evt: any) {
            let input = that.personFormGroup.get('state');
            input.setValue(this.value);
            that.person.state = this.value;
        });

        $("#personZip").inputmask({ mask: this.formats.zipCodeFormat, definitions: { 'a': { validator: '[a-zA-Z0-9 ]' } } });
        $("#personZip").on('keyup', function(evt: any) {
            let input = that.personFormGroup.get('zip');
            input.setValue(this.value);
            that.person.zip = this.value;
        });
    }

    stateSelected(event: any, group: any) {
        this.validations.stateError = '';
        this.selectedState = event.id;
        this.activeState.push(event);
        group.value.state = event.id;
    }

    onSubmit({ group, valid }: { group: FormGroup, valid: boolean }) {
        if (!valid) return;

        let personData = this.personFormGroup.value;
        let that = this;

        this.validationService.validatePhoneNumber(true, personData.phone)
        .then(function(resp: any) {
            if (!resp.isValid && that.settings.phoneAndEmailRequired) {
                that.validations.phoneError = resp.reason;
            } else {
                if(!personData.phone && !personData.email) {
                    that.validations.oneRequiredError = 'Email or phone required.';
                } else {
                    if(personData.phone && resp.reason) {
                        that.validations.phoneError = resp.reason;
                        return;
                    }

                    that.validationService.validateCountryState(personData.state ? personData.state : that.selectedState, that.settings.countryDataSettings.isStateApplicable)
                    .then(function(resp: any) {
                        if (!resp.isValid && that.settings.countryDataSettings.isStateApplicable) {
                            that.validations.stateError = resp.reason;
                        } else {
                            that.validationService.validateZipCode(personData.zip)
                            .then(function(resp: any) {
                                if (!resp.isValid) {
                                    that.validations.zipError = resp.reason;
                                } else {
                                    if (!that.person.birthDate && that.settings.personDateOfBirthRequired) {
                                        that.validations.birthDateError = 'Birth date is required';
                                    } else {
                                        that.savePerson(personData);
                                    }
                                }
                            },
                            function(error: any) {
                                that.validations.zipError = 'Unexpected error occurred. Please retry.';
                            });
                        }
                    },
                    function(error: any) {
                        that.validations.stateError = 'Unexpected error occurred. Please retry.';
                    });
                }
            }
        },
        function(error: any) {
            that.validations.phoneError = 'Unexpected error occurred. Please retry.';
        });
    }

    onDateChanged(event: any) {
        if(!event.jsdate) return;

        this.person.birthDate = event;
        this.validations.birthDateError = "";

        return;
    }

    savePerson(person: any) {
        this.person.fullName = person.title + " " + person.firstName + " " + person.lastName;
        this.person.firstName = person.firstName;
        this.person.lastName = person.lastName;
        this.person.title = person.title;
        this.person.email = person.email;
        this.person.phone = person.phone;
        this.person.address = person.address;
        this.person.city = person.city;
        this.person.state = person.state;
        this.person.zip = person.zip;
        this.person.comments = person.commments;

        this.appointment.person = this.person;
        this.sharedService.shareAppointment(this.appointment);

        let userRequest = {
            name: this.person.lastName,
            firstName: person.firstName,
            zipCode: this.person.zip
        };

        let that = this;
        this.apmService.verifyUser(userRequest).then((resp: any) => {
            if (resp.message) {
                return that.onStepReady.emit({ number: 5, apt: that.appointment });
            }

            that.currentView = 'person-data';
            that.personDataView = resp;
            console.log('person data view ', that.personDataView);
        })
        .catch((error: any) => {
            console.log(error);
            if (error === 'Cannot recognize person based on specified criteria') {
                return that.onStepReady.emit({ number: 5, apt: that.appointment });
            } else {
                that.globalError = 'Unexpected error occurred. Please retry or contact us directly.';
            }
        });
    }

    getAppointment(msg: string): void {
        if (msg) {
            this.appointment = JSON.parse(msg);
            this.person = this.appointment.person ? this.appointment.person : new Person();
        }
    }

    getSettings(msg: string): void {
        if (msg) {
            this.settings = JSON.parse(msg);
        }
    }

    getAccount(msg: string): void {
        if (msg) {
            this.account = JSON.parse(msg);
        }
    }

    onPhoneBlur(inputName: string, formName: string): void {
        let that = this;
        this.validations.phoneError = '';
        this.sValidations.phoneError = '';

        let input = this.personFormGroup.get(inputName);
        if (inputName === 'sPhone') {
            input = this.securityFormGroup.get(inputName);
        }
        this.validationService.validatePhoneNumber(inputName !== 'sPhone', input.value)
            .then(function(resp: any) {
                if (!resp.isValid && input.value.length > 0) {
                    if (formName === 'validations') {
                        that.validations.phoneError = resp.reason;
                    } else {
                        that.sValidations.phoneError = resp.reason;
                    }
                }
            }, function(err: any) {
                that.validations.phoneError = 'Unexpected error occurred. Please retry.';
            });
    }

    onStateBlur() {
        let that = this;
        let personData = this.personFormGroup.value;
        that.validationService.validateCountryState(personData.state ? personData.state : that.selectedState, that.settings.countryDataSettings.isStateApplicable)
            .then(function(resp: any) {
                 that.validations.stateError = !resp.isValid ? resp.reason : '';
            },
            function(error: any) {
                that.validations.stateError = 'Unexpected error occurred. Please retry.';
            });
    }

    onZipBlur() {
        let that = this;
        let personData = this.personFormGroup.value;
        that.validationService.validateZipCode(personData.zip)
            .then(function(resp: any) {
                that.validations.zipError = !resp.isValid ? resp.reason : '';
            },
            function(error: any) {
                that.validations.zipError = 'Unexpected error occurred. Please retry.';
            });
    }

    initForm(settings: Settings): void {
        let emailCtrl = new FormControl(this.person.email ? this.person.email : '');
        let phoneCtrl = new FormControl(this.person.phone);

        let emailValidators = new Array<any>();

        if (settings.phoneAndEmailRequired) {
            emailValidators.push(CustomValidators.mailFormat);
            emailValidators.push(Validators.required);
            emailCtrl.setValidators(emailValidators);
            phoneCtrl.setValidators(Validators.required);
        } else {
             emailCtrl.setValidators(CustomValidators.mailFormat);
        }

      let addressCtrl = new FormControl(this.person.address);
      let cityCtrl = new FormControl(this.person.city, [Validators.required, this.NoWhitespaceValidator]);
      if (settings.personAddressRequired) {
          let addressValidators = new Array<any>();
          addressValidators.push(Validators.required);
          addressValidators.push(this.NoWhitespaceValidator);
          addressCtrl.setValidators(addressValidators);
      }

      let countrySettings = settings.countryDataSettings;
      let stateCtrl = new FormControl({ value: this.person.state, disabled: !countrySettings.isStateApplicable });
      this.personFormGroup = new FormGroup({
          lastName: new FormControl(this.person.lastName, [Validators.required, this.NoWhitespaceValidator]),
          title: new FormControl(this.person.title),
          firstName: new FormControl(this.person.firstName, [Validators.required, this.NoWhitespaceValidator]),
          email: emailCtrl,
          phone: phoneCtrl,
          address: addressCtrl,
          address2: new FormControl(this.person.address2),
          city: cityCtrl,
          state: stateCtrl,
          zip: new FormControl(this.person.zip, [Validators.required, this.NoWhitespaceValidator]),

          comments: new FormControl(this.person.comments)
      });
    }

    initSecurityForm(): void {
        let phoneCtrl = new FormControl('');
        let emailCtrl = new FormControl('');
        emailCtrl.setValidators(CustomValidators.mailFormat);
        let petCtrl = new FormControl();

        this.securityFormGroup = new FormGroup({
            sPhone: phoneCtrl,
            sEmail: emailCtrl,
            sPetName: petCtrl
        });
    }

    initPasswordForm(): void {
        let newPassword = new FormControl();
        let confirmPassword = new FormControl();

        this.passwordFormGroup = new FormGroup({
            password: newPassword,
            cPassword: confirmPassword
        });
    }

    isInvalid(inputName: string): boolean {
        let input = this.personFormGroup.get(inputName);
        return !input.valid && input.touched;
    }

    showError(inputName: string, errorType: string): boolean {
        let input = this.personFormGroup.get(inputName);
        if (inputName === 'sEmail') {
            input = this.securityFormGroup.get(inputName);
        }

        return input.hasError(errorType) && input.touched;
    }

    updateTextCounter(event: any) {
      var length = this.personFormGroup.value && this.personFormGroup.value.comments ? this.personFormGroup.value.comments.length : 0;
      this.viewModel.counter = this.viewModel.maxLength - length;
    }

    checkMask(event: any, type: string) {
        switch(type) {
            case "state":
                if (!isNaN(event.key)) event.preventDefault();
                break;
            case "phone":
                if (isNaN(event.key) && event.key !== 'Backspace' && event.key !== 'Delete' && event.key !== 'Del' && event.key !== 'ArrowRight' && event.key !== 'ArrowLeft') event.preventDefault();
                break;
            default:
                break;
        }
    }

    clearValidations(field: string) {
        switch(field) {
            case 'phone':
                this.validations.phoneError = "";
                break;
            case 'email':
                this.validations.emailError = "";
                break;
            default:
                break;
        }

        this.validations.oneRequiredError = "";
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.settingsSub.unsubscribe();
    }

    startOver() {
        this.currentView = 'form-person';
    }

    proceed() {
         this.onStepReady.emit({ number: 5, apt: this.appointment });
    }

    showSecurityView() {
        this.currentView = 'security-person';
        this.initSecurityForm();

        let that = this;
        setTimeout(function() {
            $("#securityPhone").inputmask({ mask: that.formats.phoneFormat, definitions: { '9': { validator: '[0-9]' }, 'a': { validator: '[a-zA-Z]' } } });
            $("#securityPhone").on('keyup', function(evt: any) {
                let input = that.securityFormGroup.get('sPhone');
                input.setValue(this.value);
                that.person.phone = this.value;
            });
        }, 300);
    }

    onSubmitSecurity({ group, valid }: { group: FormGroup, valid: boolean }) {
        this.sValidations.phoneError = '';
        let data = this.securityFormGroup.value;
        let securityData = {
            challengeCode: this.personDataView.challengeCode,
            email: data.sEmail,
            petName: data.sPetName,
            phone: data.sPhone

        };

        let that = this;
        this.validationService.validatePhoneNumber(false, securityData.phone)
            .then(function(resp: any) {
                if (!resp.isValid && securityData.phone.length > 0) {
                    that.sValidations.phoneError = resp.reason;
                } else {
                    that.apmService.verifySecurity(securityData)
                        .then(
                            (resp: any) => {
                                console.log('correct resp ', resp);
                                if (resp.user && !resp.user.passwordDefined) {
                                    that.personToken = resp.token.accessToken;
                                    that.initPasswordForm();
                                    that.currentView = 'password-person';
                                } else {

                                    setTimeout(function() {
                                        let url = resp.user && resp.user.person ? resp.user.person.olsPortalDirectUrl : '';
                                        window.location.href = url ? url : that.redirectUrl;
                                    }, 5000);

                                    that.showRedirectMessage = true;
                                }
                            },
                            (error: any) => {
                                that.securityErrorMessage = error;
                            }
                        );
                }
            }, function(err: any) {
                that.securityErrorMessage = 'Unexpected error occurred. Please retry.';
            });

    }

    onSubmitPassword({ group, valid }: { group: FormGroup, valid: boolean }) {
        let data = this.passwordFormGroup.value;
        let passwordData = {
            password: data.password,
            confirmPassword: data.cPassword,
            currentProtectionCode: ''
        };

        let that = this;
        this.apmService.changePassword(passwordData, this.personToken)
            .then(
                (resp: any) => {
                    window.location.href = that.redirectUrl;
                },
                (error: any) => {
                    console.log('password error', error);
                    that.passwordErrorMessage = error;
                }
            );
    }

    NoWhitespaceValidator(control: FormControl) {
        if ((control.value || '').length === 0) return null;

        let isWhitespace = (control.value || '').trim().length === 0;
        let isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true };
    }

    onPaste(formField: any, event: any) {
        let input = this.personFormGroup.get(formField);
        input.setValue(event.target.value);
        this.person[formField] = event.target.value;
    }
}
