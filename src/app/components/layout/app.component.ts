import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '../../services/authService';
import { ValidationService } from '../../services/validationService';
import { SharedService } from '../../services/sharedService';

import { AuthModel } from '../../models/authModel';
import { Account } from '../../models/account';
import { Appointment } from '../../models/appointment';
import { Settings } from '../../models/settings';

import '../../../../public/css/styles.css';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html'
})

export class AppComponent {
    applicationId: string;
    locationId: string;
    forceLocation: boolean;
    application: AuthModel;
    account: Account;
    settings: Settings;
    apt: Appointment;

    errorText: String = 'Sorry, but you don\'t have permissions to use scheduler.';

    schedulerStep: number = 0;
    totalSteps: number = 6;

    displayScheduler: Boolean = true;
    isAccessible: Boolean = true;

    subscription: Subscription;


    constructor(private authService: AuthService,
                private validationService: ValidationService,
                private sharedService: SharedService
    ) {
        this.subscription = this.sharedService.appointment$.subscribe(this.getAppointment.bind(this));
    };

    ngOnInit(): void {
        let that = this;
        that.applicationId = that.getParameterByName('aid');
        this.locationId = that.getParameterByName('locationid');
        this.forceLocation = Boolean(this.getParameterByName('forcelocationid'));

        if (!this.applicationId) {
            this.hideScheduler();
            return;
        }

        this.apt = new Appointment();
        sessionStorage.setItem('appointment', JSON.stringify(this.apt));
        this.sharedService.shareAppointment(this.apt);

        this.authService.getAuthToken(this.applicationId, this.locationId, this.forceLocation).then(
            function(resp: any) {
                that.application = resp;
                that.account = resp.account;
                that.settings = resp.settings;

                console.log('Main data', resp);

                sessionStorage.setItem('token', JSON.stringify(resp.token));

                that.isAccessible = that.validationService.isSchedulerAccessible(that.application);
                that.showScheduler();

                that.sharedService.shareSettings(that.settings);
                that.sharedService.shareAccount(that.account);
            },

            function(error: any) {
                that.hideScheduler();
                that.errorText = error;
            }
        );
    }

    getParameterByName(name: string): string {
       let query = window.location.search.substring(1);
       let vars = query.split('&');

       for (let i = 0; i < vars.length; i++) {
           let pair = vars[i].split('=');
           if (pair[0].toLowerCase() === name) {
               return pair[1];
           }
       }

       return '';
    }

    showScheduler(): void {
        if (!this.isAccessible)
            this.displayScheduler = false;
    }

    hideScheduler(): void {
        this.isAccessible = false;
        this.displayScheduler = false;
    }

    selectSchedulerStep(event: any): void {
        this.schedulerStep = event.number - 1;
        this.apt = event.apt;
    }

    previousStep(step: number): void {
        if (this.schedulerStep > 0 && step === -1) {
            this.schedulerStep -= 1;
        } else if (this.schedulerStep >= step){
            this.schedulerStep = step - 1;
        }

        this.resetAppointment();
    }

    currentStep(step: number): Boolean {
        return step === this.schedulerStep + 1;
    }

    ngOnDestroy(): void {
        sessionStorage.clear();
        this.subscription.unsubscribe();
    }

    getAppointment(msg: string): void {
        if (msg) {
            this.apt = JSON.parse(msg);
        }
    }

    resetAppointment(): void {
        if (this.schedulerStep === 0) {
            this.apt.opening = null;
            this.apt.person = null;
            this.apt.provider = null;
            this.apt.providers = null;
            this.apt.pet = null;
        }

        if (this.schedulerStep === 2 && this.apt.provider) {
            this.apt.provider = null;
        }

        if (this.schedulerStep < 3 && this.apt.person) {
            this.apt.person = null;
        }

        if (this.schedulerStep < 4 && this.apt.pet) {
            this.apt.pet = null;
        }

        sessionStorage.setItem('appointment', JSON.stringify(this.apt));
        this.sharedService.shareAppointment(this.apt);
    }
};
