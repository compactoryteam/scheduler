import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '../../services/authService';
import { ValidationService } from '../../services/validationService';
import { SharedService } from '../../services/sharedService';

import { AuthModel } from '../../models/authModel';
import { Account } from '../../models/account';
import { Appointment } from '../../models/appointment';
import { Settings } from '../../models/settings';

import '../../../../public/css/styles.css';

@Component({
  selector: 'footer-app',
  templateUrl: './footer.component.html'
})

export class FooterComponent {
    account: Account;
    subscription: Subscription;

    constructor(private sharedService: SharedService
    ) {
        this.subscription = this.sharedService.account$.subscribe(this.getAccount.bind(this));
    };

    getAccount(msg: string): void {
        if (msg) {
            console.log('received account');

            this.account = JSON.parse(msg);
            this.account.webSiteUrl = "http://" +  this.account.webSite;
        }
    }
};
