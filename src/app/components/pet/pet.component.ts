import { Input, Output, AfterContentInit, ContentChild,AfterViewInit, EventEmitter, Component, ViewChild, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '../../services/authService';
import { ValidationService } from '../../services/validationService';
import { SharedService } from '../../services/sharedService';
import { PetService } from '../../services/petService';

import { AuthModel } from '../../models/authModel';
import { Account } from '../../models/account';
import { Appointment } from '../../models/appointment';
import { Settings } from '../../models/settings';
import { Pet } from '../../models/pet';

import '../../../../public/css/styles.css';

@Component({
  selector: 'pet-step',
  templateUrl: './pet.component.html'
})

export class PetComponent {
    @ViewChildren('input') vc: any;
    @Output() onStepReady: EventEmitter<any> = new EventEmitter();

    pet: Pet = new Pet();
    petFormGroup: FormGroup;
    species: any[] = [];
    breeds: any[] = [];
    genders: any[] = [];
    states: any[] = [];

    settingsSub: Subscription;

    appointment: Appointment;
    settings: Settings;
    subscription: Subscription;
    dpOptions: any = {};
    selectedDate: any;

    validations: any = {
        birthDateError: '',
        genderError: '',
        speciesError: '',
        breedError: ''
    };

    showGenderError: boolean = false;
    showSpeciesError: boolean = false;

    activeGender: Array<any> = new Array<any>();
    selectedGender: string = '';

    activeState: Array<any> = new Array<any>();
    selectedState: string = '';

    activeBreed: Array<any> = new Array<any>();
    selectedBreed: string = '';

    activeSpecies: Array<any> = new Array<any>();
    selectedSpecies: string = '';

    constructor(private sharedService: SharedService, private petService: PetService
    ) {
        this.settingsSub = this.sharedService.settings$.subscribe(this.getSettings.bind(this));
        this.subscription = this.sharedService.appointment$.subscribe(this.getAppointment.bind(this));

        this.petService.getPetBreeds().then(
            (resp: any) => {
                if (resp) {
                    for (let i = 0; i < resp.length; i++) {
                        this.breeds.push({ id: resp[i].value, text: resp[i].displayValue });
                        if (this.breeds[i].id === this.pet.breed) {
                            this.activeBreed.push(this.breeds[i]);
                        }
                    }

                    console.log(resp);
                }
            },
            (error: any) => { console.log('error happened'); }
        );

        this.petService.getPetSpecies().then(
            (resp: any) => {
                if (resp) {
                    for (let i = 0; i < resp.length; i++) {
                        this.species.push({ id: resp[i].value, text: resp[i].displayValue });
                        if (this.species[i].id === this.pet.species) {
                            this.activeSpecies.push(this.species[i]);
                        }
                    }

                    console.log(resp);
                }
            },
            (error: any) => { console.log('error happened'); }
        );
    };

    ngOnInit() {
      this.initForm(this.settings);

      let date = new Date();

        this.selectedDate = this.pet.birthDate ? this.pet.birthDate.formatted : '';

        this.dpOptions = {
            dateFormat: 'mm-dd-yyyy',
            firstDayOfWeek: 'mo',
            showClearDateBtn: false,
            height: '34px',
            width: '260px',
            showTodayBtn: false,
            selectionTxtFontSize: '14px',
            disableDays: [],
            editableDateField: false,
            customPlaceholderTxt: 'Select birth date',
            inputValueRequired: this.settings.personDateOfBirthRequired,
            disableSince: { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() }
        };

        this.petService.getPetGenders().then(
            (resp: any) => {
                if (resp) {
                    for (let i = 0; i < resp.length; i++) {
                        this.genders.push({ id: resp[i].value, text: resp[i].displayValue });
                    }
                }
             },
            (error: any) => { console.log('error happened'); }
        );

    }

    ngAfterViewInit() {
        this.vc.first.nativeElement.focus();
    }

    initForm(settings: Settings): void {

      this.petFormGroup = new FormGroup({
          name: new FormControl(this.pet.name, [Validators.required, this.NoWhitespaceValidator]),
          breed: new FormControl(this.pet.breed)
      });
    }

    onSubmit(): void {
        if(!this.selectedSpecies && !this.selectedGender) {
            this.showSpeciesError = true;
            this.showGenderError = true;
            return;
        }

        if (!this.selectedSpecies) {
            this.showSpeciesError = true;
            return;
        }

        if (!this.selectedGender) {
            this.showGenderError = true;
            return;
        }

        this.pet = this.petFormGroup.value;
        this.appointment.pet = this.pet;
        return this.onStepReady.emit({ number: 6, apt: this.appointment });
    };

    isInvalid(inputName: string): boolean {
        let input = this.petFormGroup.get(inputName);
        return !input.valid && input.touched;
    }

    showError(inputName: string, errorType: string): boolean {
        let input = this.petFormGroup.get(inputName);
        if (inputName === 'sEmail') {
            input = this.petFormGroup.get(inputName);
        }

        return input.hasError(errorType) && input.touched;
    };

    onPaste(formField: any, event: any) {
        let input = this.petFormGroup.get(formField);
        input.setValue(event.target.value);
        this.pet[formField] = event.target.value;
    }

    NoWhitespaceValidator(control: FormControl) {
        if ((control.value || '').length === 0) return null;

        let isWhitespace = (control.value || '').trim().length === 0;
        let isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true };
    }

    getSettings(msg: string): void {
        if (msg) {
            this.settings = JSON.parse(msg);
        }
    }

    getAppointment(msg: string): void {
        if (msg) {
            this.appointment = JSON.parse(msg);
            this.pet = this.appointment.pet ? this.appointment.pet : new Pet();
        }
    }

    onDateChanged(event: any) {
        if (!event.jsdate) return;

        this.pet.birthDate = event;
        this.petFormGroup.value.birthDate = event;
        this.validations.birthDateError = '';

        return;
    }

    genderSelected(event: any, group: any) {
        this.validations.stateError = '';
        this.showGenderError = false;
        this.petFormGroup.value.gender = event.target.value;
        this.selectedGender = event.target.value;
    }

    breedSelected(event: any, group: any) {
        this.validations.stateError = '';
        this.petFormGroup.value.breed = event.target.value;
    }

    speciesSelected(event: any, group: any) {
        this.showSpeciesError = false;
        this.validations.stateError = '';
        this.petFormGroup.value.species = event.target.value;
        this.selectedSpecies = event.target.value;
    }
};
