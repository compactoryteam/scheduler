import { Component, Input, EventEmitter, Output } from '@angular/core';
import { ValidationService } from '../../services/validationService';
import { AppointmentService } from '../../services/appointmentService';
import { SharedService } from '../../services/sharedService';
import { Subscription } from 'rxjs/Subscription';

import { Reason } from '../../models/reason';
import { Appointment } from '../../models/appointment';
import { Settings } from '../../models/settings';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'reason-step',
  templateUrl: './reason.component.html'
})

export class ReasonComponent {
    @Input() type: string;
    @Output() onStepReady: EventEmitter<any> = new EventEmitter();

    reasonTree: Array<Reason>;
    viewModel: Reason;
    selectedReason: Reason;
    hoveredReason: Reason;
    level: number = 0;
    settings: Settings;
    errorText: string;

    hashMap: Object = new Object();
    sanitizer: DomSanitizer;
    showSpinner: Boolean = true;
    appointment: Appointment;
    subscription: Subscription;
    settingsSub: Subscription;

    constructor(sanitizer: DomSanitizer,
                private apmService: AppointmentService,
                private validationService: ValidationService,
                private sharedService: SharedService
    ){
        this.subscription = this.sharedService.appointment$.subscribe(this.getAppointment.bind(this));
        this.settingsSub = this.sharedService.settings$.subscribe(this.getSettings.bind(this));
        this.sanitizer = sanitizer;
    }

    ngOnInit() {
        this.loadReasons();
    }

    getAppointment(msg: string): void {
        if (msg) {
            this.appointment = JSON.parse(msg);
        }
    }

    getSettings(msg: string): void {
        if (msg) {
            this.settings = JSON.parse(msg);
        }
    }

    loadReasons(): void {
        let that = this;
        this.showSpinner = true;
        this.apmService.getReasons().then(function(resp: Array<Reason>) {
            that.reasonTree = resp;

            that.viewModel = new Reason();
            that.viewModel.children = resp;

            for (let i = 0; i < that.viewModel.children.length; i++) {
                that.tooltipText(that.viewModel.children[i]);
                console.log("tooltip text here level 1");
            }

            that.showSpinner = false;
        },

        function(error: any) {
            that.errorText = 'Something wrong happened with your request. Please retry or contact us in case you will see this message again.';
            console.log(error);
        });
    }

    selectLevel(reason: Reason): void {
        if (reason.nonSelectable) return;

        this.selectedReason = reason;
        this.level = this.level + 1;
        if (reason.isCategory) {
            if (!this.hashMap[reason.id]) {
                this.hashMap[reason.id] = Object.assign({}, reason);
            }

            this.viewModel = reason;
            for (let i = 0; i < this.viewModel.children.length; i++) {
                this.tooltipText(this.viewModel.children[i]);
            }
        } else {
            this.selectReason();
        }
    }

    hoverReason(reason: Reason): void {
        if (reason.help && reason.help.isPopup && !reason.help.isSanitized) {
            let url: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(reason.help.helpUrl ? 'http://dev.appointmaster.com/pfbpatient/' + reason.help.helpUrl : '');
            reason.help.helpUrl = url;
            reason.help.isSanitized = true;
        }

        this.hoveredReason = reason.help ? reason : null;
    }

    resetReason() {
        // this.hoveredReason = null;
    }

    tooltipText(reason: Reason): void {
        if (reason.help) {
            reason.tooltipText = reason.help.helpText;
        } else {
            reason.tooltipText = '';
        }
    }

    selectReason(): void {
        let str = sessionStorage.getItem('appointment');
        let item: any;
        if (!str) {
            item = new Appointment();
        } else {
            item = JSON.parse(str);
        }

        item.reason = this.selectedReason;
        sessionStorage.setItem('appointment', JSON.stringify(item));

        this.onStepReady.next({ number: 2, apt: item });
        this.sharedService.shareAppointment(item);
    }

    returnBack(): void {
        if (!this.viewModel.parentId) {
            this.viewModel = new Reason();
            this.viewModel.children =  this.reasonTree;
            this.selectedReason = undefined;
        } else {
            this.selectedReason = Object.assign({}, this.hashMap[this.selectedReason.parentId]);
            this.viewModel = this.hashMap[this.selectedReason.id];
        }

        this.level = this.level - 1;
    }
}
