import { Component, Input, EventEmitter, Output, trigger, state, style, transition, animate } from '@angular/core';
import { ValidationService } from '../../services/validationService';
import { AppointmentService } from '../../services/appointmentService';
import { SharedService } from '../../services/sharedService';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs/Subscription';

import { Reason } from '../../models/reason';
import { Appointment } from '../../models/appointment';
import { Settings } from '../../models/settings';

@Component({
  selector: 'tree-view',
  templateUrl: './treeview.component.html',

  animations: [
    trigger('treeViewState', [
        state('expanded', style({ height: '100%' })),
        state('collapsed', style({ height: '0px' })),
        transition('collapsed => expanded', animate('1000ms ease-out')),
        transition('expanded => collapsed', animate('1000ms ease-out'))
    ])
  ]
})

export class TreeViewComponent {
    @Input() reasons: Reason;
    @Input() level: number;
    @Output() onSelect: EventEmitter<any> = new EventEmitter();
    currentId: string = '';
    hoveredReason: Reason;
    sanitizer: DomSanitizer;
    appointment: Appointment;
    subscription: Subscription;
    settingsSub: Subscription;
    selectedReason: Reason;
    treeViewState: String = 'collapsed';
    settings: Settings;

    private expHashMap: Object = new Object();
    private treeState: Array<string> = new Array<string>();

    constructor(sanitizer: DomSanitizer,
                private apmService: AppointmentService,
                private validationService: ValidationService,
                private sharedService: SharedService
    ){
        this.sanitizer = sanitizer;
        this.sharedService.treeState$.subscribe(this.getTreeState.bind(this));
        this.subscription = this.sharedService.appointment$.subscribe(this.getAppointment.bind(this));
        this.settingsSub = this.sharedService.settings$.subscribe(this.getSettings.bind(this));
    }

    ngOnInit(): void {
        // Recover tree state on view init
        // Recover root
        if (!this.reasons) {
            this.expHashMap[this.treeState[0]] = { isOpen: true, level: 0 };
        }

        // Recover childrens
        if (this.treeState.length > 0 && this.reasons) {
            for (let i = 0; i < this.treeState.length; i++) {
                if (this.reasons.children) {
                    for (let j = 0; j < this.reasons.children.length; j++) {
                        let rs = this.reasons.children[j];
                        this.tooltipText(rs);
                        if (rs.id === Number(this.treeState[i]) && rs.isCategory) {
                            this.expHashMap[rs.id] = { isOpen: true, level: this.level };
                        }
                    }
                }
            }
        }

        if (this.reasons && this.reasons.children) {
            for (let i = 0; i < this.reasons.children.length; i++) {
                let rs = this.reasons.children[i];
                this.tooltipText(rs);
            }
        }
    }

    getAppointment(msg: string): void {
        if (msg) {
            this.appointment = JSON.parse(msg);
            this.selectedReason = this.appointment.reason;
        }
    }

    getSettings(msg: string): void {
        if (msg) {
            this.settings = JSON.parse(msg);
        }
    }

    toggleChildren(event: any, reason: Reason): void {
        if (reason.nonSelectable) return;
        if (Number(event.target.id) !== reason.id) return;
        if (reason.isCategory) {
            this.updateTreeState(event.target.id);
        }

        this.resetReasons(event.target.id);

        if (reason.isCategory) {
            this.currentId = event.target.id;
            let isOpen = this.expHashMap[this.currentId] && this.expHashMap[this.currentId].isOpen;
            this.expHashMap[this.currentId] = { isOpen: !isOpen, level: this.level };

            this.treeViewState = isOpen ? 'collapsed' : 'expanded';
        } else {
            this.selectReason(reason);
        }
    }

    isExpanded(reason: Reason): Boolean {
        return this.expHashMap[reason.id] && this.expHashMap[reason.id].isOpen && reason.isCategory;
    }

    isDisabled(reason: Reason): Boolean {
        return !reason.help || reason.help.isPopup;
    }

    resetReasons(id: string): void {
        if (this.expHashMap[id] && this.expHashMap[id].isOpen) return;

        for (let key in this.expHashMap) {
            if (this.expHashMap[key].level === this.level && this.expHashMap[key].isOpen) {
                this.expHashMap[key].isOpen = false;
            }
        }
    }

    tooltipText(reason: Reason): void {
        if (reason.help) {
            reason.tooltipText = reason.help.helpText;
        } else {
            reason.tooltipText = '';
        }
    }

    selectReason(reason: Reason): void {
        let str = sessionStorage.getItem('appointment');
        let item: any;
        if (!str) {
            item = new Appointment();
        } else {
            item = JSON.parse(str);
        }

        item.reason = reason;
        sessionStorage.setItem('appointment', JSON.stringify(item));

        this.onSelect.next({ number: 2, apt: item });
        this.sharedService.shareAppointment(item);
    }

    hoverReason(reason: Reason): void {
        if (reason.help && reason.help.isPopup && !reason.help.isSanitized) {
            let url: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(reason.help.helpUrl ? 'http://dev.appointmaster.com/pfbpatient/' + reason.help.helpUrl : '');
            reason.help.helpUrl = url;
            reason.help.isSanitized = true;
        }

        this.hoveredReason = reason.help ? reason : null;
    }

    resetReason() {
        // this.hoveredReason = null;
    }

    getTreeState(msg: string) {
        if (msg) {
            this.treeState = JSON.parse(msg);
        }
    }

    updateTreeState(id: string): void {
        if (this.level === 0) {
            this.treeState = new Array<string>();
        }

        let isExist = this.expHashMap[id];
        let isOpened = isExist && this.expHashMap[id].isOpen;
        if (!isOpened) {
            let index = this.treeState[this.treeState.length - 1];
            if (this.expHashMap[index]) {
                let lastLevel = this.expHashMap[index].level;
                if (this.level === lastLevel) {
                    this.treeState.pop();
                }
            }

            this.treeState.push(id);
        } else {
            this.treeState.pop();
        }

        this.sharedService.shareTreeState(this.treeState);
    }
}
