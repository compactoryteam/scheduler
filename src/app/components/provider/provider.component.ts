import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AppointmentService } from '../../services/appointmentService';
import { SharedService } from '../../services/sharedService';

import { Appointment } from '../../models/appointment';
import { Provider } from '../../models/provider';

@Component({
  selector: 'provider-step',
  templateUrl: './provider.component.html'
})

export class ProviderComponent {
    @Output() onStepReady: EventEmitter<any> = new EventEmitter();

    subscription: Subscription;
    appointment: Appointment;
    providers: Array<Provider>;
    hoveredProvider: Provider;

    isError: Boolean = false;
    errorText: String = '';

    constructor(private apmService: AppointmentService,
                private sharedService: SharedService) {
                    this.subscription = this.sharedService.appointment$.subscribe(this.getAppointment.bind(this));
                }

    ngOnInit() {
        this.providers = new Array<Provider>();
        this.loadProvider();
    }

    getAppointment(msg: string): void {
        if (msg) {
            this.appointment = JSON.parse(msg);
        }
    }

    loadProvider() {
        let that = this;
        let providers = this.appointment.providers;

        for(let i = 0; i < providers.length; i++) {
            if(!providers[i]) {
                providers.splice(i, 1);
                i--;
            }
        }

        for(let i = 0; i < providers.length; i++) {
            this.apmService.getProvider(providers[i]).then(function(resp: Provider) {
                that.providers.push(resp);

                if(providers.length === 1)
                    that.selectProvider(resp);
            },
            function(error: any) {
                that.isError = true;
                that.errorText = 'Something wrong happened with your request. Please retry or contact us in case you will see this message again.'
                console.log(error);
            });
        }

        if(providers.length === 0) {
            this.selectProvider(new Provider());
        }
    }

    hoverProvider(provider: Provider) {
        this.hoveredProvider = provider;
    }

    selectProvider(provider: Provider) {
        let item = this.appointment ? this.appointment : JSON.parse(sessionStorage.getItem('appointment'));
        item.provider = provider;

        sessionStorage.setItem('appointment', JSON.stringify(item));

        this.onStepReady.emit({ number: 4, apt: item });
        this.sharedService.shareAppointment(item);
    }
}
