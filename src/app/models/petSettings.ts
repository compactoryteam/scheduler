import { ValueModel } from './valueModel';

export class PetSettings {
    species: ValueModel[];
    genders: ValueModel[];
    appointmentMaxPetsAllowed: number;
};
