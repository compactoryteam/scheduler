export class Opening {
    id: number;
    dateTime: Date;
    providerId: number;
    columnId: string;
    columnName: string;
    viewString: string;
    text: string;
}
