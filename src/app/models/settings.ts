export class Settings {
    usesPetPortal: Boolean;
    allowAppointmentRequest: Boolean;
    availableToNewPerson: Boolean;
    availableToExistingPerson: Boolean;
    reasonSelectionMode: string;
    reasonSelectionMainTitle: string;
    resaonSelectionSecondaryTitle: string;
    phoneAndEmailRequired: Boolean;
    personDateOfBirthRequired: Boolean;
    personAddressRequired: Boolean;
    allowBoardingRequest: Boolean;
    boardingCaptureCageSharing: Boolean;
    creationOfNewPetAllowed: Boolean;
    allowRefillRequest: Boolean;
    openingSelectionByCalendar: Boolean;
    allowPrintingCertificates: Boolean;
    textMessageLegalOptInIsMandatory: Boolean;
    countryDataSettings: {
        zipCodeLabel: string;
        stateLabel: string;
        isStateApplicable: Boolean;
        zipCodeFormat: string;
        stateFormat: string;
        phoneFormat: string;
        states: Array<any>;
    };
};
