import { Opening } from './opening';

export class OpeningModel {
    continuationToken: string;
    inProgress: Boolean;
    openings: Array<Opening>;
}
