export class Pet {
    name: string;
    species: string;
    breed: string;
    gender: string;
    photoUrl: string;
    hasPhoto: boolean;
    birthDate: any;
}
