export class Provider {
    name: string;
    firstName: string;
    displayName: string;
    photoUrl: string;
    hasPhoto: Boolean;
}
