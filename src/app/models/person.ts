export class Person {
    firstName: string;
    lastName: string;
    title: string;
    email: string;
    phone: string;
    address: string;
    address2: string;
    city: string;
    state: string = '';
    zip: string;
    comments: string;
    birthDate: any;
    fullName: string;
}