export class Account {
    accountNumber: number;
    name: string;
    address: string;
    addressLine2: string;
    city: string;
    state: string;
    zipCode: string;
    webSite: string;
    market: string;
    countryCode: string;
    canUseOls: Boolean;
    specificOlsUrl: string;
    logoUrl: string;
    webSiteUrl: string;
};
