export class Location {
    id: string;
    name: string;
    address: string;
    addressLine2: string;
    city: string;
    state: string;
    zipCode: string;
    phone: string;
    webSite: string;
    email: string;
    logoUrl: string;
};