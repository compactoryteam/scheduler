import { Token } from './token';
import { Account } from './account';
import { Settings } from './settings';
import { PetSettings } from './petSettings';
import { Location } from './location';

export class AuthModel {
    token: Token;
    account: Account;
    settings: Settings;
    petDataSettings: PetSettings;
    location: Location;
};
