export class Reason {
    id: number;
    name: string;
    isCategory: Boolean;
    children: Array<Reason>;
    parentId: number;
    isOpened: Boolean;
    help: any;
    nonSelectable: Boolean;
    nonSelectableText: string;
    tooltipText: string;
};
