import { Reason } from './reason';
import { Opening } from './opening';
import { Provider } from './provider';
import { Person } from './person';
import { Pet } from './pet';

export class Appointment {
    reason: Reason;
    opening: Opening;
    provider: Provider;
    providers: Array<any>;
    person: Person;
    pet: Pet;
}
