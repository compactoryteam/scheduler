import { OpaqueToken } from '@angular/core';

export let APP_CONFIG = new OpaqueToken('app.config');

export interface IAppConfig {
  apiEndpoint: string;
  rootEndpoint: string;
}

export const appConfig: IAppConfig = {
  apiEndpoint: 'https://api.dev.appointmaster.com/ols/v1',
  rootEndpoint: 'http://apmscheduler.herokuapp.com'
};
