import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
    selector: '[schedulerProgress]',
})
export class ProgressDirective {
    @Input('schedulerProgress') progressWidth: number;
    @Input() total: number;

    constructor(private el: ElementRef) {}

    ngOnChanges(): void {
        this.setWidth();
    }

    setWidth(): void {
        let icon: any = this.el.nativeElement.firstElementChild;
        if (this.progressWidth > 0) {
            let width: number = this.el.nativeElement.parentElement.offsetWidth;
            let currentStepWidth: number = width / this.total;

            currentStepWidth = currentStepWidth * this.progressWidth;

            this.el.nativeElement.style.width = 60 + currentStepWidth + 'px';
            icon.style.visibility = 'visible';
        } else {
            this.el.nativeElement.style.width = '60px';
            icon.style.visibility = 'visible';
        }
    }
}
