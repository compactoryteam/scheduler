import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
    selector: '[schedulerSteps]',
})
export class StepsDirective {
    @Input('schedulerSteps') progressWidth: number;
    @Input() total: number;

    steps: Array<any> = new Array();

    constructor(private el: ElementRef) {}

    ngOnChanges(): void {
        this.setSteps();
    }

    setSteps(): void {
        let allChildren: Array<any> = this.el.nativeElement.children;

        for (let i = 0; i < allChildren.length; i++) {
            if (i % 2 === 0) {
                this.steps.push(allChildren[i]);
            }
        }

        this.refreshSteps();

        this.setActiveStep();
        this.setDoneStep();
    }

    refreshSteps(): void {
        for (let i = 0; i < this.steps.length; i++) {
            let child = this.steps[i].children[0];
            child.className = '';
        }
    }

    setActiveStep(): void {
        let child = this.steps[this.progressWidth].children[0];
        let hasActive = false;
        for (let i = 0; i < child.classList.length; i++) {
            if (child.classList[i] === 'active') {
                hasActive = true;
                break;
            }
        }

        if (!hasActive) {
            child.className += ' active';
        }
    }

    setDoneStep(): void {
        let stepDone = this.progressWidth - 1;
        if (stepDone >= 0) {
            for (let j = 0; j < this.progressWidth; j++) {
                let child = this.steps[j].children[0];
                let hasDone = false;
                for (let i = 0; i < child.classList.length; i++) {
                    if (child.classList[i] === 'done') {
                        hasDone = true;
                        break;
                    }
                }

                if (stepDone >= 0 && !hasDone) {
                    child.className += ' done';
                }
            }
        }


    }
}
