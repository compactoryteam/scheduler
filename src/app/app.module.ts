import './rxjs-extensions';

import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

// third-party
import { MyDatePickerModule } from './3rdparty/my-date-picker/my-date-picker.module';
import { TooltipModule } from 'ng2-tooltip';
import { PopoverModule } from 'ng2-popover';
import { SelectModule } from 'ng2-select';

// inner components
import { AppComponent } from './components/layout/app.component';
import { FooterComponent } from './components/layout/footer.component';
import { ReasonComponent } from './components/reason/reason.component';
import { OpeningComponent } from './components/opening/opening.component';
import { TreeViewComponent } from './components/treeview/treeview.component';
import { ProviderComponent } from './components/provider/provider.component';
import { PersonComponent } from './components/person/person.component';
import { PetComponent } from './components/pet/pet.component';

// inner services
import { APP_CONFIG, appConfig } from './app.config';
import { AuthService } from './services/authService';
import { ValidationService } from './services/validationService';
import { HttpClient } from './services/httpService';
import { AppointmentService } from './services/appointmentService';
import { SharedService } from './services/sharedService';
import { PetService } from './services/petService';

// inner directives
import { ProgressDirective } from './directives/progress.directive';
import { StepsDirective } from './directives/steps.directive';

// inner helpers
import { OpeningHelper } from './components/opening/opening.helper';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    MyDatePickerModule,
    TooltipModule,
    PopoverModule,
    SelectModule
  ],
  declarations: [
    AppComponent,
    FooterComponent,
    ReasonComponent,
    OpeningComponent,
    TreeViewComponent,
    ProviderComponent,
    PersonComponent,
    PetComponent,
    ProgressDirective,
    StepsDirective
  ],
  providers: [
      AuthService,
      ValidationService,
      HttpClient,
      AppointmentService,
      SharedService,
      PetService,
      OpeningHelper,
      { provide: APP_CONFIG, useValue: appConfig }
  ],
  bootstrap: [ AppComponent, FooterComponent ]
})

export class AppModule { }
