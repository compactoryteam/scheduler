import { Injectable, Inject }    from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { APP_CONFIG, IAppConfig } from '../app.config';

@Injectable()
export class HttpClient {
    http: Http;

    constructor(http: Http) {
        this.http = http;
    }

    createAuthorizationHeader(headers: Headers, tokenString?: string): void {
        let token = sessionStorage.getItem('token');
        if (token) {
            let value = tokenString && tokenString.length > 0 ? tokenString : JSON.parse(token).accessToken;
            headers.append('Authorization', 'Bearer ' + value);
        }
        headers.append('Content-type', 'application/json');
    }

    refreshAuthorizationToken(): void {

    }

    get(url: string) {
        let headers = new Headers();

        this.createAuthorizationHeader(headers);

        return this.http.get(url, { headers: headers })
                        .toPromise()
                        .then((res: Response) => {
                            try {
                                return res.json();
                            } catch (err) {
                                return res;
                            }
                        })
                        .catch(this.handleError);
    }

    post(url: string, obj: any) {
        let data = JSON.stringify(obj);
        let headers = new Headers();
        this.createAuthorizationHeader(headers);

        return this.http.post(url, data, { headers: headers })
                        .toPromise()
                        .then((res: Response) => {
                            try {
                                return res.json();
                            } catch (err) {
                                return res;
                            }
                        })
                        .catch(this.handleError);
    }

    put(url: string, obj: any, singleToken?: string) {
        let data = JSON.stringify(obj);
        let headers = new Headers();
        this.createAuthorizationHeader(headers, singleToken);

        return this.http.put(url, data, { headers: headers })
                    .toPromise()
                    .then((res: Response) => {
                        try {
                            return res.json();
                        } catch (err) {
                            return res;
                        }
                    })
                    .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
      console.error('An error occurred', error);
      let obj: any;
      try {
            obj = JSON.parse(error._body);
            error = new Error(obj.message);
      } catch (ex) {
            error = new Error('Unexpected error occured. Please retry or contact us directly.');
      }

      if (obj && obj.message === 'Cannot recognize person based on specified criteria' && obj.code === 'ols.unrecognized_person') return obj;

      return Promise.reject(error);
    }
}
