import { Injectable, Inject }    from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { HttpClient } from './httpService';

import { AuthModel } from '../models/authModel';
import { APP_CONFIG, IAppConfig } from '../app.config';

@Injectable()
export class ValidationService {
     private baseUrl =  this.config.apiEndpoint;

    constructor(@Inject(APP_CONFIG) private config: IAppConfig, private http: HttpClient) { }

    isSchedulerAccessible(model: AuthModel): Boolean {
        let useOls = model.account.canUseOls;
        let allowAppointment = model.settings.allowAppointmentRequest;

        return useOls && allowAppointment && model.settings.availableToNewPerson && this.isDentalAccessible(model);
    };

    isDentalAccessible(model: AuthModel): Boolean {
        if (!model.settings.creationOfNewPetAllowed) {
            return model.account.market === "DENTAL";
        }

        return true;
    }

    validatePhoneNumber(checkSms: Boolean, phone: string): any {
        checkSms = false;
        phone = phone ? phone : 'undefined';
        const url = `${this.baseUrl}/validations/phone?checkSmsCapable=${checkSms}&value=${phone}`;
        return this.http.get(url);
    };

    validateCountryState(countryState: string, isApplicable: Boolean): any {
        if (isApplicable) {
            countryState = countryState ? countryState : 'undefined';
            const url = `${this.baseUrl}/validations/countryState?value=${countryState}`;
            return this.http.get(url);
        } else {
            return Promise.resolve({ isValid: true });
        }
    };

    validateZipCode(zipCode: string): any {
        zipCode = zipCode ? zipCode : 'undefined';
        const url = `${this.baseUrl}/validations/zipCode?value=` + zipCode;
        return this.http.get(url);
    };
}
