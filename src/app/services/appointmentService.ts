import { Injectable, Inject }    from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Reason } from '../models/reason';

import 'rxjs/add/operator/toPromise';

import { APP_CONFIG, IAppConfig } from '../app.config';
import { HttpClient } from './httpService';

@Injectable()
export class AppointmentService {
    private baseUrl =  this.config.apiEndpoint;

    constructor(@Inject(APP_CONFIG) private config: IAppConfig, private http: HttpClient) { }

    getReasons(): Promise<Array<Reason>> {
        const url = `${this.baseUrl}/appointmentReasons?depth=999`;
        return this.http.get(url);
    }

    getOpenings(reasonId: number, fromDate: Date, toDate: Date, cToken: string): any {
        let url = `${this.baseUrl}/searchOpenings`;

        if (cToken) {
            url += '?continuationToken=' + cToken;
            return this.http.get(url);
        } else {
            let body = {
                reasonId: reasonId,
                fromDate: fromDate,
                toDate: toDate
            };
            return this.http.post(url, body);
        }
    }

    getProvider(providerId: number): any {
        const url = `${this.baseUrl}/providers/` + providerId;
        return this.http.get(url);
    }

    verifyUser(user: any): any {
        let url = `${this.baseUrl}/connectionChallenges`;
        return this.http.post(url, user);
    }

    verifySecurity(security: any) {
        let url = `${this.baseUrl}/connections/_withChallenge`;
        return this.http.post(url, security);
    }

    changePassword(password: any, singleToken: string) {
        let url = `${this.baseUrl}/users/me/password`;
        return this.http.put(url, password, singleToken);
    }
}
