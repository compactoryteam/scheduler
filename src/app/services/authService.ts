import { Injectable, Inject }    from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { HttpClient } from './httpService';

import { AuthModel } from '../models/authModel';
import { APP_CONFIG, IAppConfig } from '../app.config';

@Injectable()
export class AuthService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private authUrl =  this.config.apiEndpoint;

  constructor(@Inject(APP_CONFIG) private config: IAppConfig, private http: HttpClient) { }

  getAuthToken(appId: string, locationId: string, forceLocationId: boolean): Promise<AuthModel> {
    const url = `${this.authUrl}/connections`;
    return this.http.post(url, { aid: appId, locationId: locationId, forceLocationId: forceLocationId });
  }
}
