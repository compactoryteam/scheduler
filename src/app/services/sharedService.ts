import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Appointment } from '../models/appointment';
import { Settings } from '../models/settings';
import { Account } from '../models/account';

@Injectable()
export class SharedService {
    _appointment$ = new BehaviorSubject('');
    appointment$ = this._appointment$.asObservable();

    _treeState$ = new BehaviorSubject('');
    treeState$ = this._treeState$.asObservable();

    _settings$ = new BehaviorSubject('');
    settings$ = this._settings$.asObservable();

    _account$ = new BehaviorSubject('');
    account$ = this._account$.asObservable();



    shareAppointment(apt: Appointment) {
        let msg = JSON.stringify(apt);
        this._appointment$.next(msg);
    }

    shareTreeState(state: any) {
        let msg = JSON.stringify(state);
        this._treeState$.next(msg);
    }

    shareSettings(settings: Settings) {
        let msg = JSON.stringify(settings);
        this._settings$.next(msg);
    }

    shareAccount(account: Account) {
        let msg = JSON.stringify(account);
        this._account$.next(msg);
    }
}
