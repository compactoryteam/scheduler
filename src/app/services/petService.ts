import { Injectable, Inject }    from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { HttpClient } from './httpService';

import { AuthModel } from '../models/authModel';
import { APP_CONFIG, IAppConfig } from '../app.config';

@Injectable()
export class PetService {
    private baseUrl =  this.config.apiEndpoint;

    constructor(@Inject(APP_CONFIG) private config: IAppConfig, private http: HttpClient) { }

    getPetSpecies(): any {
        const url = `${this.baseUrl}/lists/petSpecies`;
        return this.http.get(url);
    }

    getPetBreeds(): any {
         const url = `${this.baseUrl}/lists/petBreeds`;
        return this.http.get(url);
    }

    getPetGenders(): any {
         const url = `${this.baseUrl}/lists/petGenders`;
        return this.http.get(url);
    }
}